#!/usr/bin/env python
# coding: utf-8

# In[28]:


from sklearn import tree
import pydotplus
import pandas as pd
from IPython.display import Image


# In[29]:


temperature_annual = pd.read_csv('Temperature-change-annual.csv')
temperature_annual


# In[30]:


dummi_data = temperature_annual.iloc[:, 0:4].values
dummi_data


# In[31]:


temperature_annual = temperature_annual[['Year', 'Temperature departure','Warmest year ranking',
                                                   'Climate','Climate feel']]
temperature_annual


# In[32]:


temperature_data = pd.get_dummies(read_temperature_annual)
temperature_data


# In[33]:


clf = tree.DecisionTreeClassifier()
clf_train = clf.fit(temperature_data, temperature_annual['Climate'])
clf_train = clf.fit(temperature_data, temperature_annual['Climate feel'])


# In[34]:


dot_data = tree.export_graphviz(clf_train, out_file=None, feature_names=list(temperature_data.columns.values), 
                                class_names=['Cold', 'Normal cool', 'Hot', 'High cool', 'Normal hot', 
                                             'High hot'],
                                rounded=True, filled=True)

graph = pydotplus.graph_from_dot_data(dot_data)
Image(graph.create_png())

